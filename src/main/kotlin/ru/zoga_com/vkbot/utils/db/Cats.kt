package ru.zoga_com.vkbot.utils.db

import ru.zoga_com.vkbot.utils.Database
import java.sql.ResultSet

object Cats {
    fun getCats(peer: String): ResultSet {
        val res: ResultSet = Database().get("SELECT killed FROM cats WHERE scope = \"${peer}\"")
        res.next()
        return res
    }
}