package ru.zoga_com.vkbot.utils

import org.yaml.snakeyaml.Yaml
import java.io.File
import java.io.FileWriter
import java.io.InputStream
import java.io.FileInputStream

class YamlParser {
    val yaml: Yaml = Yaml()
    companion object {
        @JvmStatic
        var mapOfConfig: Map<String, String> = mapOf()
    }

    fun initConfig() {
        if(!File("config.yml").exists()) {
            File("config.yml").createNewFile()
            val writer: FileWriter = FileWriter(File("config.yml"))
            writer.write("#токен бота\ntoken: ''\n#ключ для api.hypixel.net\nkey: ''\n#настройки mysql\nmysql-user: ''\nmysql-password: ''\nmysql-database: ''\nmysql-address: ''")
            writer.close()
            println("Конфиг не найден, поэтому был создан.")
        }
        val stream: InputStream = FileInputStream(File("config.yml"))
        mapOfConfig = yaml.load(stream)
    }

    fun getConfigValue(key: String): String {
        return mapOfConfig.getOrDefault(key, "no")
    }
}