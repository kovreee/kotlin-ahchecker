package ru.zoga_com.vkbot.commands

import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.utils.Database
import ru.zoga_com.vkbot.utils.Utils
import ru.zoga_com.vkbot.utils.db.Users
import api.longpoll.bots.model.events.messages.MessageNew
import java.sql.ResultSet
import java.util.regex.Pattern

object SetNameCommand: Command() {
    override fun onCommand(args: Array<String>, message: MessageNew) {
        try {
            if(args.size == 2) {
                val result: ResultSet = Users.getUser(message.getMessage().getFromId(), "nick")
                if(Utils().createUserIfNotExists(result, message)) {
                    if(!Pattern.compile("[^(A-Z0-9a-z|_|+)]").matcher(args[1]).find()) {
                        if(Utils().checkPlayerExists(args[1])) {
                            Users.setNick(message.getMessage().getFromId(), args[1])
                            Utils().sendMessage("@id${message.getMessage().getFromId()} (Ваш) ник изменен на ${args[1]}", message.getMessage().getPeerId())
                        } else {
                            Utils().sendMessage("Такой ник не существует.", message.getMessage().getPeerId())
                        }
                    } else { 
                        Utils().sendMessage("Ник содержит недопустимые символы.", message.getMessage().getPeerId())
                    }
                } else {
                    Utils().sendMessage("Ваш профиль не найден, поэтому сейчас он будет создан.\n\nВыполните команду снова.", message.getMessage().getPeerId())
                }
            }
        } catch(e: Exception) {
            Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName}: ${e.message} (Повторите попытку снова)", message.getMessage().getPeerId())
            e.printStackTrace()
        }
    }
}