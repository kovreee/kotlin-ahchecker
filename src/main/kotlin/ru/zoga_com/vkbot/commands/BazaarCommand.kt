package ru.zoga_com.vkbot.commands

import api.longpoll.bots.LongPollBot
import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.utils.Utils
import api.longpoll.bots.model.events.messages.MessageNew
import java.awt.image.BufferedImage
import java.awt.Graphics2D
import java.awt.Color
import java.awt.Font
import java.awt.FontMetrics
import javax.imageio.ImageIO
import ru.zoga_com.vkbot.utils.ThreadPoolService
import java.io.File
import java.util.UUID
import java.awt.RenderingHints
import java.text.DecimalFormat

object BazaarCommand: Command() {
    val cordsX: List<Int> = listOf(520, 520, 520, 230, 810)
    val cordsY: List<Int> = listOf(240, 330, 410, 560, 560)
    val point: List<String> = listOf("name", "buyPrice", "sellPrice", "buyVolume", "sellVolume")
    val fontSizes: List<Float> = listOf(72.0f, 52.0f, 52.0f, 30.0f, 30.0f)
    val colors: List<String> = listOf("#88c0d0", "#e5e9f0", "#e5e9f0", "#e5e9f0", "#e5e9f0")
    val texts: List<String> = listOf("", "Buy: ", "Sell: ", "Buy volume: ", "Sell volume: ")
    val format: DecimalFormat = DecimalFormat("#,###.0")
    val font: Font = Font.createFont(Font.TRUETYPE_FONT, this::class.java.getResourceAsStream("/JetBrains.ttf"))
    @JvmStatic
    var products: HashMap<String, String> = hashMapOf()

    override fun onCommand(args: Array<String>, message: MessageNew) {
        try {
            if(args.size == 2) {
                ThreadPoolService.createTask(Runnable() {
                val randomFile = File("images/" + UUID.randomUUID().toString() + ".png")
                val image: BufferedImage = BufferedImage(1040, 640, BufferedImage.TYPE_INT_RGB)
                val graphic: Graphics2D = image.createGraphics()
                graphic.setPaint(Color.decode("#2e3440"))
                graphic.fillRect(0, 0, 1040, 640)
                var product: String?
                when(products.containsKey(args[1])) {
                    true -> product = products.get(args[1])
                    false -> product = args[1]
                }
                for(i in 0..4) {
                    graphic.setColor(Color.decode(colors.get(i)))
                    graphic.setFont(font.deriveFont(Font.PLAIN, fontSizes.get(i)))
                    graphic.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB)
                    var text: String
                    when(i) {
                        1 -> text = texts.get(i) + format.format((Utils().getBazaarItem(Utils().getJson("https://sky.lea.moe/api/v2/bazaar"), product + "." + point.get(i))).toDouble())
                        2 -> text = texts.get(i) + format.format((Utils().getBazaarItem(Utils().getJson("https://sky.lea.moe/api/v2/bazaar"), product + "." + point.get(i))).toDouble())
                        else -> text = texts.get(i) + Utils().getBazaarItem(Utils().getJson("https://sky.lea.moe/api/v2/bazaar"), product + "." + point.get(i))
                    }
                    val xPosition: Int = cordsX.get(i) - graphic.getFontMetrics().getStringBounds(text, graphic).getWidth().toInt() / 2
                    graphic.drawString(text, xPosition, cordsY.get(i))
                }
                ImageIO.write(image, "PNG", randomFile)
                println("Записана картинка под именем " + randomFile.name)
                Utils().sendMessageWithPhoto((randomFile.name).toString().replace(".png", "", true), message.getMessage().getPeerId(), randomFile)
                randomFile.deleteOnExit()
            })
            }
        } catch(e: Exception) {
            Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName}: ${e.message} (Предмет не существует?)", message.getMessage().getPeerId())
            e.printStackTrace()
        }
    }

    fun fillProducts() {
        products.put("eq", "ENCHANTED_QUARTZ")
        products.put("en", "ENCHANTED_NETHERRACK")
        products.put("bc", "BOOSTER_COOKIE")
        products.put("tara", "TARANTULA_WEB")
        products.put("grav", "GRAVEL")
        products.put("epotato", "ENCHANTED_POTATO")
        products.put("eslime", "ENCHANTED_SLIME_BALL")
        products.put("ebirch", "ENCHANTED_BIRCH_LOG")
        products.put("emelon", "ENCHANTED_MELON")
        products.put("esugar", "ENCHANTED_SUGAR")
        products.put("erod", "ENCHANTED_BLAZE_ROD")
        products.put("ecake", "ENCHANTED_CAKE")
        products.put("ecobble", "ENCHANTED_COBBLESTONE")
        products.put("esnow", "ENCHANTED_SNOW_BLOCK")
        products.put("rabbit", "RABBIT_FOOT")
        products.put("ecactus", "ENCHANTED_CACTUS_GREEN")
        products.put("endstone", "ENCHANTED_ENDSTONE")
        products.put("ecookie", "ENCHANTED_COOKIE")
        products.put("esand", "ENCHANTED_SAND")
        products.put("estring", "ENCHANTED_STRING")
        products.put("eacacia", "ENCHANTED_ACACIA_LOG")
        products.put("elapis", "ENCHANTED_LAPIS_LAZULI")
        products.put("ecocoa", "ENCHANTED_COCOA")
        products.put("eleather", "ENCHANTED_LEATHER")
        products.put("esponge", "ENCHANTED_SPONGE")
        products.put("eink sack", "ENCHANTED_INK_SACK")
        products.put("espruce", "ENCHANTED_SPRUCE_LOG")
        products.put("eflesh erotten", "ENCHANTED_ROTTEN_FLESH")
        products.put("epork", "ENCHANTED_PORK")
        products.put("redstoneblock", "ENCHANTED_REDSTONE_BLOCK")
        products.put("elamp", "ENCHANTED_REDSTONE_LAMP")
        products.put("elava", "ENCHANTED_LAVA_BUCKET")
        products.put("eiron", "ENCHANTED_IRON")
        products.put("eglowblock", "ENCHANTED_GLOWSTONE_BLOCK")
        products.put("edoak", "ENCHANTED_DARK_OAK_LOG")
        products.put("egold ingot", "ENCHANTED_GOLD")
        products.put("echicken", "ENCHANTED_RAW_CHICKEN")
        products.put("elily", "ENCHANTED_WATER_LILY")
        products.put("cata", "CATALYST")
        products.put("hcata", "HYPER_CATALYST")
        products.put("edust", "ENCHANTED_GLOWSTONE_DUST")
        products.put("ecane", "ENCHANTED_SUGAR_CANE")
        products.put("eferm", "ENCHANTED_FERMENTED_SPIDER_EYE")
        products.put("egold block", "ENCHANTED_GOLD_BLOCK")
        products.put("ejungle", "ENCHANTED_JUNGLE_LOG")
        products.put("eclay", "ENCHANTED_CLAY_BALL")
        products.put("rev", "REVENANT_FLESH")
        products.put("scomp", "SUPER_COMPACTOR_3000")
        products.put("ehay", "ENCHANTED_HAY_BLOCK")
        products.put("epaper", "ENCHANTED_PAPER")
        products.put("ebone", "ENCHANTED_BONE")
        products.put("ediablock", "ENCHANTED_DIAMOND_BLOCK")
        products.put("supfrag", "SUPERIOR_FRAGMENT")
        products.put("efoot", "ENCHANTED_RABBIT_FOOT")
        products.put("hpb", "HOT_POTATO_BOOK")
        products.put("eice", "ENCHANTED_ICE")
        products.put("oldfrag", "OLD_FRAGMENT")
        products.put("eobsidian", "ENCHANTED_OBSIDIAN")
        products.put("ecoal", "ENCHANTED_COAL")
        products.put("equartz", "ENCHANTED_QUARTZ")
        products.put("ewet", "ENCHANTED_WET_SPONGE")
        products.put("efish", "ENCHANTED_RAW_FISH")
        products.put("ecow", "ENCHANTED_RAW_BEEF")
        products.put("eslimeblock", "ENCHANTED_SLIME_BLOCK")
        products.put("efeather", "ENCHANTED_FEATHER")
        products.put("eoak", "ENCHANTED_OAK_LOG")
        products.put("ecarrot", "ENCHANTED_CARROT")
        products.put("epumpkin", "ENCHANTED_PUMPKIN")
        products.put("ecooked", "ENCHANTED_COOKED_FISH")
        products.put("ecream", "ENCHANTED_MAGMA_CREAM")
        products.put("emutton", "ENCHANTED_MUTTON")
        products.put("eblaze", "ENCHANTED_BLAZE_POWDER")
        products.put("sum", "SUMMONING_EYE")
        products.put("ebaked", "ENCHANTED_BAKED_POTATO")
        products.put("edia", "ENCHANTED_DIAMOND")
        products.put("rec", "RECOMBOBULATOR_3000")
        products.put("min", "REFINED_MINERAL")
        products.put("eqb", "ENCHANTED_QUARTZ_BLOCK")
        products.put("eq", "ENCHANTED_QUARTZ")
    }
}
