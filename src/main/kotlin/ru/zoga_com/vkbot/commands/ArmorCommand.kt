package ru.zoga_com.vkbot.commands

import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.utils.Utils
import api.longpoll.bots.model.events.messages.MessageNew
import kotlin.text.uppercase

object ArmorCommand: Command() {
    override fun onCommand(args: Array<String>, message: MessageNew) { 
        if(args.size == 2) {
            try {
                val armor: HashMap<String, String> = Utils().getArmor(args[1]) 
                val text: String = "👗 Броня игрока ${args[1]}:\n\n🕶 [${armor.get("helmetRarity").toString().uppercase()}] ${armor.get("helmet")}\n👕 [${armor.get("chestplateRarity").toString().uppercase()}] ${armor.get("chestplate")}\n👖 [${armor.get("leggingsRarity").toString().uppercase()}] ${armor.get("leggings")}\n🧦 [${armor.get("bootsRarity").toString().uppercase()}] ${armor.get("boots")}"
                Utils().sendMessage(text, message.getMessage().getPeerId())
            } catch(e: Exception) {
                Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName}: ${e.message} (Игрок не существует?)", message.getMessage().getPeerId())
                e.printStackTrace()
            }
        }
    }
}