package ru.zoga_com.vkbot.commands

import java.util.Timer
import java.util.concurrent.TimeUnit
import java.io.File
import kotlin.random.Random
import ru.zoga_com.vkbot.utils.Chains
import api.longpoll.bots.model.events.messages.MessageNew
import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.utils.Utils

object ChainsCommand: Command() {
    val photos: MutableList<String> = mutableListOf()
    override fun onCommand(args: Array<String>, message: MessageNew) {
        try {
            if(args.size == 1) {
                fillPhotos()
                Utils().sendMessageWithPhoto(Chains.generate(), message.getMessage().getPeerId(), File("chain-images/${photos.get(Random.nextInt(0, (photos.size - 1)))}"))
            }
        } catch(e: Exception) {
            Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName}: ${e.message} (Повторите попытку снова)", message.getMessage().getPeerId()) 
            e.printStackTrace() 
        }
    }

    fun fillPhotos() {
        photos.clear()
        for(file: File in File("chain-images/").listFiles()) {
            if(file.getName().endsWith(".jpg")) { photos.add(file.getName()) }
        }
    }
}
