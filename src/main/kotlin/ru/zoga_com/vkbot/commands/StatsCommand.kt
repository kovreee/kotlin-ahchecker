package ru.zoga_com.vkbot.commands

import ru.zoga_com.vkbot.types.Command
import ru.zoga_com.vkbot.types.User
import ru.zoga_com.vkbot.utils.Utils
import ru.zoga_com.vkbot.utils.Database
import ru.zoga_com.vkbot.utils.db.Users
import api.longpoll.bots.model.events.messages.MessageNew
import org.json.JSONObject
import java.text.DecimalFormat
import java.sql.ResultSet

object StatsCommand: Command() {
    val format: DecimalFormat = DecimalFormat("#.00")
    val formatKarma: DecimalFormat = DecimalFormat("#,###")

    override fun onCommand(args: Array<String>, message: MessageNew) {
        try {
            when(args.size) {
                2 -> {  
                    val player: JSONObject = JSONObject(Utils().getJson("https://api.slothpixel.me/api/players/${args[1]}"))
                    var text: String
                    when(player.get("last_login")) {
                        is Long -> text = "Статистика игрока ${player.getString("username")}:\n\nУровень: ${player.getDouble("level")}\nКарма: ${formatKarma.format(player.getLong("karma"))}\nОчков ачивок: ${player.getLong("achievement_points")}\n\nПервый вход: ${Utils().formatTime(player.getLong("first_login"))}\nПоследний вход: ${Utils().formatTime(player.getLong("last_login"))}\n\nПоследнее обновление статистики: ${Utils().formatTime(player.getLong("last_logout"))}"
                        else -> text = "Статистика игрока ${player.getString("username")}:\n\nУровень: ${player.getDouble("level")}\nКарма: ${formatKarma.format(player.getLong("karma"))}\nОчков ачивок: ${player.getLong("achievement_points")}\n\nПервый вход: ${Utils().formatTime(player.getLong("first_login"))}\nПоследний вход: Скрыто.\n\nПоследнее обновление статистики: Скрыто."
                    }
                    Utils().sendMessage(text, message.getMessage().getPeerId())
                }
                1 -> {
                    if(Utils().createUserIfNotExists(Users.getUser(message.getMessage().getFromId(), "cock_length"), message)) {
                        if(User(message.getMessage().getFromId().toLong()).nick != "false") {
                            val player: JSONObject = JSONObject(Utils().getJson("https://api.slothpixel.me/api/players/${User(message.getMessage().getFromId().toLong()).nick}"))
                            var text: String
                            when(player.get("last_login")) {
                                is Long -> text = "Статистика игрока ${player.getString("username")}:\n\nУровень: ${player.getDouble("level")}\nКарма: ${formatKarma.format(player.getLong("karma"))}\nОчков ачивок: ${player.getLong("achievement_points")}\n\nПервый вход: ${Utils().formatTime(player.getLong("first_login"))}\nПоследний вход: ${Utils().formatTime(player.getLong("last_login"))}\n\nПоследнее обновление статистики: ${Utils().formatTime(player.getLong("last_logout"))}"
                                else -> text = "Статистика игрока ${player.getString("username")}:\n\nУровень: ${player.getDouble("level")}\nКарма: ${formatKarma.format(player.getLong("karma"))}\nОчков ачивок: ${player.getLong("achievement_points")}\n\nПервый вход: ${Utils().formatTime(player.getLong("first_login"))}\nПоследний вход: Скрыто.\n\nПоследнее обновление статистики: Скрыто."
                            }
                            Utils().sendMessage(text, message.getMessage().getPeerId())
                        } else { Utils().sendMessage("Вы ещё не установили ник.", message.getMessage().getPeerId()) }
                    } else { Utils().sendMessage("Ваш профиль не найден, поэтому сейчас он будет создан.\n\nВыполните команду снова.", message.getMessage().getPeerId()) }
                }
            }
        } catch(e: Exception) {
            Utils().sendMessage("Произошла ошибка: ${e.javaClass.canonicalName}: ${e.message} (Игрок не существует?)", message.getMessage().getPeerId())
            e.printStackTrace()
        }
    }
}
